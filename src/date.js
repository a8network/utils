'use strict';

const moment = require('moment');
const dateFormat = 'YYYY-MM-DD';
const dateTimeFormat = 'YYYY-MM-DD HH:mm';
const fullDateTimeFormat = 'YYYY-MM-DD HH:mm:ss';

class DateTime {
    static getCurrentDate(dateTime) {
        return moment(dateTime).format(dateFormat);
    }
    static getTodayUtcDateTimeString() {
        return moment().utc().format(dateTimeFormat);
    }
    static getSameDayInLastMonth(dateTime, subMonth = 1) {
        return moment(dateTime).subtract(subMonth, 'months').format(dateFormat);
    }
    static startOfMonth(date) {
        return moment(date).startOf('month').format(dateFormat);
    }
    static endOfMonth(date) {
        return moment(date).endOf('month').format(dateFormat);
    }
}

module.exports = DateTime;
