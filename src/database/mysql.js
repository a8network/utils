'use strict';

const mysql = require('mysql');

module.exports = class MySql {
    constructor(host, user, password, database, port = 3306, connectionLimit = 10) {
        if (!this.pool) {
            this.pool = mysql.createPool({
                connectionLimit, host, user, password, database
            });
        }
    }
    getPool() {
        return this.pool;
    }
}
