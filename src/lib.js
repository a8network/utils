'use strict';

const tokenIssuer = require('./authentication/token-issuer');
const authorization = require('./authentication/authorization');
const Request = require('./request');
const DBAdapter = require('./database/adapter');
const DateTimeUtils = require('./date');
const regexHelper = require('./helpers/regex');
const userHelper = require('./helpers/user');
const siteHelper = require('./helpers/site');
const LoopUtils = require('./utils/loop');
const Mysql = require('./database/mysql');
const Redis = require('./database/redis');

class Utils {
    static config(config) {
        Utils.tokenSecret = config.tokenSecret;
        Utils.apiPath = config.apiPath;
        Utils.authorizationApi = config.authorizationApi;
        Utils.regexHelper = regexHelper;
        Utils.userHelper = userHelper;
        Utils.siteHelper = siteHelper;
    }

    static getTokenIssuerInstance() {
        if (Utils.tokenIssuerInstance) {
            return Utils.tokenIssuerInstance;
        }
        Utils.tokenIssuerInstance = tokenIssuer.getInstance(Utils.tokenSecret);
        return Utils.tokenIssuerInstance;
    }

    static getAuthorizationInstance() {
        if (Utils.authorizationInstance) {
            return Utils.authorizationInstance;
        }
        Utils.authorizationInstance = new authorization.Authorization();
        return Utils.authorizationInstance;
    }

    static getDBAdapterInstance() {
        return DBAdapter;
    }

    static getMySqlInstance() {
        return Mysql;
    }
    static getRedisInstance() {
        return Redis;
    }

    static getDateTimeInstance() {
        return DateTimeUtils;
    }
    
    static getRequestInstance() {
        if (Utils.requestInstance) {
            return Utils.requestInstance;
        }
        Utils.requestInstance = Request.getInstance(Utils.apiPath, Utils.tokenSecret);
        return Utils.requestInstance;
    }

    static getLoopInstance() {
        return LoopUtils;
    }

}

module.exports = Utils;

