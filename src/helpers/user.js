'use strict';

module.exports = {
	accountTypes: {
		PUBLISHER: 'publisher',
		ADVERTISER: 'advertiser',
		MANAGER: 'manager',
	},
	accountStates: {
		PENDING: 'pending',
		ACTIVATED: 'activated',
		INACTIVE: 'inactive',
		BANNED: 'banned',
	},
	roles: {
		ROOT: 'root',
		ADMINISTRATOR: 'administrator',
		REPORTER: 'reporter',
		SELLER: 'seller',
		SUPPORTER: 'supporter'
	}
}
