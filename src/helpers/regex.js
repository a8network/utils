'use strict';

module.exports = {
    objectId: /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,
    password: /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]+$/,
};
