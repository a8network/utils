'use strict';

module.exports = {
    siteStates: {
        "PENDING": 'pending',
        "ACTIVATED": "activated",
        "DEACTIVATED": "deactivated"
    }
};
