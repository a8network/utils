'use strict';

const req = require('request');
const TokenIssuer = require('./authentication/token-issuer');

module.exports = class Request {

	constructor(hostname, tokenSecret) {
		this.hostname = hostname;
		this.tokenSecret = tokenSecret;
		if (this.tokenSecret) {
			this.tokenIssuer = TokenIssuer.getInstance(tokenSecret);
		}
	}

	/**
	 * Send GET request
	 * 
	 * @param {String} resource 
	 * 
	 * @returns {Promise}
	 */
	get(resource) {
		return this._send('get', resource);
	}

	/**
	 * Send POST request
	 * 
	 * @param {String} resource
	 * @param {object} payload
	 * 
	 * @returns {Promise}
	 */
	post(resource, payload) {
		return this._send('post', resource, payload);
	}

	/**
	 * Send PATCH request
	 * 
	 * @param {String} resource
	 * @param {object} payload
	 * 
	 * @returns {Promise}
	 */
	patch(resource, payload) {
		return this._send('patch', resource, payload);
	}
	/**
	 * Send PATCH request
	 * 
	 * @param {String} resource
	 * @param {object} payload
	 * 
	 * @returns {Promise}
	 */
	delete(resource, payload) {
		return this._send('delete', resource, payload);
	}

	/**
	 * Sends authorized request
	 * 
	 * @param {String} method
	 * @param {String} resource
	 * @param {object} payload
	 * 
	 * @return {Promise}
	 * 
	 * @private
	 */

	_send(method, resource, payload) {
		let options = {};
		if (this.tokenSecret) {
			options = this.tokenIssuer.getRequestHeaders();
		}
		options.url = (this.hostname || '') + resource;
		if (payload) {
			options.body = payload;
		}

		return new Promise((resolve, reject) => {
			req[method](options, (err, response) => (
				err || !response.statusCode.toString().startsWith(2)
					? reject(err || response.body) : resolve(response.body)
			));
		});
	}

	static getInstance(hostname, tokenSecret) {
		if (this.instance) {
			return this.instance;
		}
		this.instance = new Request(hostname, tokenSecret);
		return this.instance;
	}
}
