'use strict';
const jsonWebToken = require('jsonwebtoken');

class TokenIssuer {
	
	constructor(tokenSecret) {
		this.tokenSecret = tokenSecret;
	}
	issueToken(userId, options) {
		if (!userId && this.cachedSuperToken) {
			return this.cachedSuperToken;
		}
		var token = jsonWebToken.sign({ userId: userId || 'SuperUser' },
			this.tokenSecret, options || { noTimestamp: true });
		if (!userId) {
			this.cachedSuperToken = token;
		}
		return token;
	}
	decodeToken(token) {
		return jsonWebToken.verify(token, this.tokenSecret);
	}

	getRequestHeaders(userId) {
		const token = this.issueToken(userId);
		const headerOptions = {
			json: true,
			headers: {
				Authorization: `Bearer ${token}`,
			},
		};
		return headerOptions;
	}
	verifyToken(token, encodingSecret, ignoreExpiration) {
		let decodeToken = '';
		try {
			decoded = ignoreExpiration ?
				jsonWebToken.verify(token, encodingSecret, { ignoreExpiration: true }) :
				jsonWebToken.verify(token, encodingSecret);
		} catch (err) {
			decoded = '';
		}
		return decoded;
	}

	generateToken(encodingData, encodingSecret, options) {
		const tokenSecret = encodingSecret || this.tokenSecret;		
		return options
			? jsonWebToken.sign(encodingData, tokenSecret, options)
			: jsonWebToken.sign(encodingData, tokenSecret);
	}

	static getInstance(tokenSecret) {
		if (TokenIssuer.instance) {
			return TokenIssuer.instance;
		}
		TokenIssuer.instance = new TokenIssuer(tokenSecret);
		return TokenIssuer.instance;
	}
}

module.exports = TokenIssuer;
