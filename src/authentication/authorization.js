'use strict';

function Authorization(settings) {
	const self = this;
}

'use strict';

const unauthorizedError = { code: 401, message: 'Unauthorized access to the resource!' };

function Authorization(settings) {
  const self = this;
  const serviceSettings = settings || { accessApiRoles: { superUser: 'SuperUser' } };
  self.accessApiRoles = serviceSettings.accessApiRoles;
  self.authorizationMethods = { response: 1, request: 2 };
  self.serviceSettings = serviceSettings;
  self.authorizeAPIHandler = typeof serviceSettings.authorizeAPIHandler === 'function' ?
    serviceSettings.authorizeAPIHandler : defaultAuthorizeAPIHandler;

  self.isCredentialSuperUser = (credential) => self.accessApiRoles.superUser === credential;

  self.validateToken = (request, decodedToken, callback) => {
    const route = request.route;
    const apiUrlPrefix = self.serviceSettings.apiUrlPrefix;
    const isApiUrl = route.path.indexOf(apiUrlPrefix) === 0;

    const credential = decodedToken.userId;
    // Super user got full access
    if (self.isCredentialSuperUser(credential)) {
      return callback(null, true, decodedToken);
    }
    //  Request to API
    if (isApiUrl) {
      self.authorizeAPIHandler(credential, (success, apiConsumer) => {
        if (success) {
          return callback(null, true, apiConsumer);
        }
        return callback(null, false, decodedToken);
      });
    } else {
      return callback(null, true, decodedToken);
    }
  };

  self.requestValidation = (request, reply) => {
    if (!request.auth.isAuthenticated) {
      return reply.continue();
    }
    const authorizationMethods = self.authorizationMethods;
    const route = request.route;
    const routeAuthorizationSettings = route.settings.plugins.authorization;
    const validationFunction = getRouteValidationFunction(routeAuthorizationSettings,
      authorizationMethods.request);
    const userId = request.auth.credentials.userId;
    const isSuperUser = self.isCredentialSuperUser(userId);

    if (!isSuperUser && validationFunction) {
      validationFunction(request.payload, userId, request.params).then((validated) => {
        if (!validated) {
          return reply({ message: unauthorizedError.message }).code(unauthorizedError.code);
        }
        return reply.continue();
      });
    } else {
      return reply.continue();
    }
    return null;
  };

  self.responseValidation = (request, reply) => {
    if (!request.auth.isAuthenticated) {
      return reply.continue();
    }
    const authorizationMethods = self.authorizationMethods;
    const response = request.response;
    const route = request.route;
    const routeAuthorizationSettings = route.settings.plugins.authorization;
    const validationFunction = getRouteValidationFunction(routeAuthorizationSettings,
      authorizationMethods.response);
    const userId = request.auth.credentials.userId;
    const isSuperUser = self.isCredentialSuperUser(userId);
    const responseBody = response.source;

    if (!isSuperUser && response.statusCode === 200 && validationFunction) {
      validationFunction(responseBody, userId, request.params).then((validated) => {
        if (!validated) {
          return reply({ message: unauthorizedError.message }).code(unauthorizedError.code);
        }
        return reply.continue();
      });
    } else {
      return reply.continue();
    }
    return null;
  };

  self.superUserOnlyAuthorization = (payload, userId) => {
    const accessApiRoles = self.accessApiRoles;
    return new Promise((resolve) => {
      resolve(accessApiRoles.superUser === userId);
    });
  };

  self.userAuthorization = (payload, userId, params) =>
    new Promise((resolve) => {
      const userIdFromParameters = payload && payload.userId ? payload.userId : params.userId;
      resolve(!userIdFromParameters || userIdFromParameters === userId);
    });

  function defaultAuthorizeAPIHandler(credential, callback) {
    const allApiRoles = self.accessApiRoles;

    // No role list is provided
    if (!allApiRoles) {
      return callback({ authorized: true });
    }

    for (const key in allApiRoles) {
      if (allApiRoles[key] === credential) {
        return callback({ authorized: true });
      }
    }
    callback({ authorized: false });
  }

  function getRouteValidationFunction(routeAuthorizationSettings, validationMethod) {
    if (!routeAuthorizationSettings) {
      return null;
    }
    if (!routeAuthorizationSettings.validationMethod) {
      if (validationMethod === self.authorizationMethods.request) {
        return routeAuthorizationSettings.validate;
      }
    } else if (routeAuthorizationSettings.validationMethod === validationMethod) {
      return routeAuthorizationSettings.validate;
    }
    return null;
  }

  self.superUserOnlyValidation = (request, decodedToken, callback) => {
    const superUserApiRole = self.accessApiRoles.superUser;
    const isSuperUser = superUserApiRole === decodedToken.userId;
    return callback(null, isSuperUser, decodedToken);
  };
}

exports.Authorization = Authorization;
